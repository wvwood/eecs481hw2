EECS481 HW2

Usage:

name button: displays the name of the author of this homework.

uniqname button: displays the uniqname of the author of this homework.

store this message button: stores the message entered in the text field along with the
date it was entered and starts a new activity displaying the past five messages stored.
In this new activity the back button takes the user back to the main activity.