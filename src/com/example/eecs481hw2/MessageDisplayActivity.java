package com.example.eecs481hw2;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MessageDisplayActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_display);
		
		Button backButton = (Button)findViewById(R.id.backButton);
		backButton.setOnClickListener(backButtonClickListener);
		
		Intent mainIntent = getIntent();
		messages = mainIntent.getStringArrayListExtra(MainActivity.MAIN_TYPED_MESSAGES);
		
		int start = (messages.size() <= 5) ? 0 : messages.size() - 5;
		String display = new String();
		
		for (int i = start; i < messages.size(); i++) {
			display += messages.get(i) + "\n";
		}
		
		EditText displayEditText = (EditText) findViewById(R.id.messagesEditText);
		displayEditText.setKeyListener(null);
		displayEditText.setText(display);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_display, menu);
		return true;
	}
	
	private Button.OnClickListener backButtonClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			finish();
		}
	};
	
	private ArrayList<String> messages;

}
