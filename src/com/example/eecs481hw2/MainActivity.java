package com.example.eecs481hw2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	public final static String MAIN_TYPED_MESSAGES = "com.example.eecs481hw2.MAIN_TYPED_MESSAGE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initMainScreenButtons();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void initMainScreenButtons() {
		Button nameButton = (Button)findViewById(R.id.nameButton);
		nameButton.setOnClickListener(nameButtonClickListener);
		
		Button uniqNameButton = (Button)findViewById(R.id.unameButton);
		uniqNameButton.setOnClickListener(uniqNameButtonClickListener);
		
		Button storeMessageButton = (Button)findViewById(R.id.storeMessageButton);
		storeMessageButton.setOnClickListener(storeButtonClickListener);
	}
	
	private Button.OnClickListener nameButtonClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			TextView nameView = getNameTextView();
			nameView.setText(getString(R.string.my_name));
			
		}
	};
	
	private Button.OnClickListener uniqNameButtonClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			TextView nameView = getNameTextView();
			nameView.setText(getString(R.string.my_uniq_name));
		}
	};
	
	private Button.OnClickListener storeButtonClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(MainActivity.this, MessageDisplayActivity.class);
			EditText editText = (EditText)findViewById(R.id.messageEditText);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd HH:mm:ss");
			String currentDate = dateFormat.format(Calendar.getInstance().getTime());
			
			messages.add(currentDate + " " + editText.getText().toString());
			editText.setText("");
			
			intent.putStringArrayListExtra(MAIN_TYPED_MESSAGES, (ArrayList<String>)messages);
			startActivity(intent);
		}
	};
	
	private TextView getNameTextView() {
		return (TextView)findViewById(R.id.nameTextView);
	}
	
	private List<String> messages = new ArrayList<String>();

}
